#ifndef GAME_H_
#define GAME_H_

#include "SDL2Common.h"
#include "Player.h"

typedef struct Game {
    static const int WINDOW_WIDTH = 800;
    static const int WINDOW_HEIGHT= 600;

    // Declare window and renderer objects
    SDL_Window*	     gameWindow = nullptr;
    SDL_Renderer*    gameRenderer = nullptr;

    // Texture which stores the actual sprite (this
    // will be optimised).
    SDL_Texture*     backgroundTexture = nullptr;
    
    //Declare Player
    Player player;

    // Window control 
    bool quit = false;  //false
    
    // Keyboard
    const Uint8 *keyStates;

} Game;

bool initGame(Game* game);
void runGameLoop(Game* game);
void processInputs(Game* game);
void updateGame(Game* game, float timeDelta);
void drawGame(Game* game);
void cleanUpGame(Game* game);

#endif